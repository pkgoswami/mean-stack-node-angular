var express = require("express"),
	mongoose = require("mongoose"),
	bodyParser = require("body-parser");

var db = mongoose.connect('mongodb://localhost/movieStore');

var app = express();
var port = process.env.PORT || 3000;

app.use(express.static(__dirname+'/client'));
//loading the middleware
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

Genre = require('./models/genre');
Movie = require('./models/movie');

app.get('/', function (req, res){
	res.send("Hello from the Server !!");
});

// Get Genres data from Server
app.get('/api/genres', function(req, res){
	Genre.getGenres(function (err, genres){
		if(err){
			res.status(500).send(err);
		}
		res.json(genres);
	});
});


// Add Genre
app.post('/api/genres', function(req, res){
	var genre = req.body;
	Genre.addGenre(genre, function (err, genre){
		if(err){
			res.status(500).send(err);
		}
		res.json(genre);
	});
});

// Update Genre
app.put('/api/genres/:_id', function(req, res){
	var id = req.params._id;
	var genre = req.body;
	Genre.updateGenre(id, genre, {}, function (err, genre){
		if(err){
			res.status(500).send(err);
		}
		res.json(genre);
	});
});

// Delete Genre
app.delete('/api/genres/:_id', function(req, res){
	var id = req.params._id;
	var genre = req.body;
	Genre.deleteGenre(id, genre, {}, function (err, genre){
		if(err){
			res.status(500).send(err);
		}
		res.json(genre);
	});
});



// Get Movies Data from Server
app.get('/api/movies', function(req, res){
	Movie.getMovies(function (err, movies){
		if(err){
			res.status(500).send(err);
		}
		res.json(movies);
	});
});

// Get Movie from Server
app.get('/api/movies/:_id', function(req, res){
	Movie.getMovieById(req.params._id, function (err, movie){
		if(err){
			res.status(500).send(err);
		}
		res.json(movie);
	});
});

// Add Movie
app.post('/api/movies', function(req, res){
	var movie = req.body;
	Movie.addMovie(movie, function (err, movie){
		if(err){
			res.status(500).send(err);
		}
		res.json(movie);
	});
});

// Update Movie
app.put('/api/movies/:_id', function(req, res){
	var id = req.params._id;
	var movie = req.body;
	Movie.updateMovie(id, movie, {}, function (err, movie){
		if(err){
			res.status(500).send(err);
		}
		res.json(movie);
	});
});

// Delete Genre
app.delete('/api/movies/:_id', function(req, res){
	var id = req.params._id;
	var movie = req.body;
	Movie.deleteMovie(id, movie, {}, function (err, movie){
		if(err){
			res.status(500).send(err);
		}
		res.json(movie);
	});
});


app.listen(port, function (){
	console.log("Running on port " + port);
});